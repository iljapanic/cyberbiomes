const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);
const slugify = require('slugify');

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;
  let slug;

  if (node.internal.type === `Airtable` && node.table === `glossary`) {
    slug = slugify(node.data.term, { lower: true });
    createNodeField({ node, name: `slug`, value: slug });
  } else if (node.internal.type === `Airtable` && node.table === `projects`) {
    slug = slugify(node.data.title, { lower: true });
    createNodeField({ node, name: `slug`, value: slug });
  }
};

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  return new Promise((resolve, reject) => {
    graphql(`
      {
        glossary: allAirtable(filter: { table: { eq: "glossary" } }) {
          edges {
            node {
              data {
                term
              }
              fields {
                slug
              }
            }
          }
        }
        projects: allAirtable(filter: { table: { eq: "projects" } }) {
          edges {
            node {
              data {
                title
              }
              fields {
                slug
              }
            }
          }
        }
      }
    `).then(result => {
      result.data.glossary.edges.forEach(edge => {
        createPage({
          path: `lexicon/${edge.node.fields.slug}`,
          component: path.resolve(`./src/templates/glossary.js`),
          context: {
            term: edge.node.data.term
          }
        });

        result.data.projects.edges.forEach(edge => {
          createPage({
            path: `projects/${edge.node.fields.slug}`,
            component: path.resolve(`./src/templates/project.js`),
            context: {
              title: edge.node.data.title
            }
          });
        });
      });

      resolve();
    });
  });
};
