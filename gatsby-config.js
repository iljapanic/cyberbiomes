require(`dotenv`).config({
  path: `.env.${process.env.NODE_ENV}`
});

module.exports = {
  pathPrefix: '/cyberbiomes',
  siteMetadata: {
    title: `Cyberbiomes`,
    description: `exploring concepts, projects and initiatives at the intersection of nature, culture and technology`,
    author: `@iljapanic`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-modal-routing`,
      options: {
        modalProps: {
          overlayClassName: 'modalOverlay',
          className: 'modalContent'
        }
      }
    },
    {
      resolve: `gatsby-plugin-postcss`,
      options: {
        postCssPlugins: [
          require(`postcss-import`),
          require(`postcss-custom-media`),
          require(`postcss-css-variables`),
          require(`postcss-hexrgba`),
          require(`postcss-color-function`),
          require(`postcss-calc`),
          require(`autoprefixer`)
        ]
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images/`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `text`,
        path: `${__dirname}/src/text/`
      }
    },
    {
      resolve: `gatsby-source-airtable`,
      options: {
        apiKey: process.env.AIRTABLE_API_KEY,
        tables: [
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: `glossary`,
            tableView: `sorted`,
            queryName: `glossary`,
            mapping: { definition: `text/markdown` },
            tableLinks: [`references`]
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: `references`,
            tableView: `sorted`,
            queryName: `references`,
            mapping: { preview: `fileNode` },
            tableLinks: [`authors`, `organizations`, `keywords`]
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: `authors`,
            tableView: `sorted`,
            queryName: `authors`,
            tableLinks: [`keywords`, `references`]
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: `organizations`,
            tableView: `sorted`,
            queryName: `organizations`,
            tableLinks: [`keywords`, `references`]
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: `keywords`,
            tableView: `sorted`,
            queryName: `keywords`
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: `projects`,
            tableView: `published`,
            queryName: `projects`,
            mapping: { gallery: `fileNode`, notes: `text/markdown`, cover_image: `fileNode` },
            tableLinks: [`keywords`, `authors`, `organizations`]
          }
        ]
      }
    },
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: ['IBM Plex Mono:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i']
        }
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: 'gatsby-remark-external-links',
            options: {
              target: '_blank',
              rel: null
            }
          }
        ]
      }
    }
  ]
};
